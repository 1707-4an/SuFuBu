#include "launcher.h"
#include "game.h"
#include "ui_launcher.h"

Launcher::Launcher(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::Launcher)
{
    ui->setupUi(this);
}

Launcher::~Launcher()
{
    delete ui;
}

QSize Launcher::getRes()
{
    QSize gameSize;

    if(ui->widescreen->isChecked())
    {
        switch(ui->screenRes->currentIndex())
        {
            case 0:
                gameSize.setWidth(1920);
                gameSize.setHeight(1080);
                break;
            case 1:
                gameSize.setWidth(1600);
                gameSize.setHeight(900);
                break;
        }
    }
    else
    {
        switch(ui->screenRes->currentIndex())
        {
            case 0:
                gameSize.setWidth(1600);
                gameSize.setHeight(1200);
                break;
            case 1:
                gameSize.setWidth(1400);
                gameSize.setHeight(1050);
                break;
            case 2:
                gameSize.setWidth(1152);
                gameSize.setHeight(864);
                break;
            case 3:
                gameSize.setWidth(1024);
                gameSize.setHeight(768);
                break;
            case 4:
                gameSize.setWidth(800);
                gameSize.setHeight(600);
                break;
            case 5:
                gameSize.setWidth(640);
                gameSize.setHeight(480);
                break;
        }
    }

    return gameSize;
}

void Launcher::on_play_clicked()
{
    this->close();

    Game* game = new Game();

    game->resize(getRes());

    if(this->ui->fullscreen->isChecked())
        game->showFullScreen();
    else
        game->show();

    game->initOpenGL();
}

void Launcher::on_widescreen_clicked()
{
    ui->screenRes->clear();

    ui->screenRes->addItem("1920x1080");
    ui->screenRes->addItem("1600x900");
}

void Launcher::on_nowidescreen_clicked()
{
    ui->screenRes->clear();

    ui->screenRes->addItem("1600x1200");
    ui->screenRes->addItem("1400x1050");
    ui->screenRes->addItem("1152x864");
    ui->screenRes->addItem("1024x768");
    ui->screenRes->addItem("800x600");
    ui->screenRes->addItem("640x480");
}

void Launcher::on_exit_clicked()
{
    this->close();
}
