#ifndef LAUNCHER_H
#define LAUNCHER_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class Launcher; }
QT_END_NAMESPACE

class Launcher : public QMainWindow
{
    Q_OBJECT

public:
    Launcher(QWidget *parent = nullptr);
    ~Launcher();

private slots:
    void on_play_clicked();

    void on_widescreen_clicked();

    void on_nowidescreen_clicked();

    void on_exit_clicked();

private:
    Ui::Launcher *ui;

    QSize getRes();
};
#endif // LAUNCHER_H
