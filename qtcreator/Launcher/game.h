#ifndef GAME_H
#define GAME_H

#include <QWidget>
#include <QOpenGLWidget>

namespace Ui {
class Game;
}

class Game : public QWidget
{
    Q_OBJECT
public:
    explicit Game(QWidget *parent = nullptr);
    ~Game();

    void initOpenGL();
    void play();

private slots:

private:
    Ui::Game *ui;
};

#endif // GAME_H
