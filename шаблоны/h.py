import os
import numpy as np
#import random
#import pandas as pd
#import matplotlib.pyplot as plt
#import scipy.special
'''m2 = np.loadtxt('m2.txt')
x = np.arange(10)
y = np.arange(10)
plt.imshow(m2) #создает картинки
plt.show() #открывает окно с картинкой
os.system("pause")'''
arr = np.array()
i=1
while True:
    print(f'Вводите количество предметов в партии {i} :')
    a = int(input())
    if a == 0:
        break
    else:
        arr[i] = a
        i+=1

''' class neural:
    #(себя, входные узлы, скрытые узлы, выходные узлы, скорость обучения)
    def __init__(self, inputnodes, hiddennodes, outputnodes, learningrate):
        #задать кол-во узлов во входном, скрытом и выходном слое
        self.inodes = inputnodes
        self.hnodes = hiddennodes
        self.onodes = outputnodes
        # матрицы весовых коэффициэнтов  связей wih и who. весов коэф связей между узлом i и j
        self.wih = np.random.normal(0.0, pow(self.hnodes, - 0.5), (self.hnodes, self.inodes))
        self.who = np.random.normal(0.0, pow(self.onodes, - 0.5), (self.onodes, self.hnodes))
        #коэффициэнт обучения
        self.lr = learningrate
        # использование сигмоиы в качествее функции активации
        self.activation_function = lambda x: scipy.special.expit(x)
        pass
    def train(self, inputs_list, targets_list):
        # преобразовать список входных значений в двухмерный массив 
        inputs = np.array(inputs_list, ndmin=2).T
        targets = np.array(targets_list, ndmin=2).T
        # рассчитать входящие сигналы для скрытого слоя 
        hidden_inputs = np.dot(self.wih, inputs)
        # рассчитать исходящие сигналы для скрытого слоя 
        hidden_outputs = self.activation_function(hidden_inputs)
        # рассчитать входящие сигналы для выходного слоя 
        final_inputs = np.dot(self.who, hidden_outputs)
        # рассчитать исходящие сигналы для выходного слоя 
        final_outputs = self.activation_function(final_inputs)
        # ошибка = целевое значение - фактичское значение
        output_errors = targets - final_outputs
        #  ошибки скрытого слоя - это ошибки output_errors, распределенные пропорционально весовым коэффициентам связей и рекомбинированные на скрытых узлах 
        hidden_errors = np.dot(self.who.T, output_errors)
        # обновить всовые коэфффициэнты связей между скрытым и выходным слоями
        self.who += self.lr * np.dot((output_errors * final_outputs * (1.0 - final_outputs)), np.transpose(hidden_outputs))
        # обновим весовые коэффициэнты связей между входным и скрытым слоями
        self.wih += self.lr * np.dot((hidden_errors * hidden_outputs * (1.0 - hidden_outputs)), np.transpose(inputs))
        
        pass
    def query(self, inputs_list):
        # преобразовать список входных значений в двумрный массив
        inputs = np.array(inputs_list, ndmin=2).T
        # расчитываем входящие сигналы для скрытого слоя
        hidden_inputs = np.dot(self.wih, inputs)
        # расчитываем исходящие сигналы для скрытого слоя
        hidden_outputs = self.activation_function(hidden_inputs)
        # расчитаем входящие сигналы для выходного слоя
        final_inputs = np.dot(self.who, hidden_outputs)
        # расчитаем исходящие сигналы для выходного слоя
        final_outputs = self.activation_function(final_inputs)

        return final_outputs '''








# тест 
'''input_nodes = 3
hidden_nodes = 3
output_nodes = 3
learning_rate = 0.3
n = neural(input_nodes, hidden_nodes,output_nodes,learning_rate)
n.query([1.0, 0.5, -1.5])
print(n.query([1.0, 0.5, -1.5]))'''






''' f = open('m1.txt') 
m1 = [line.replace("\n\n", "").split() for line in f]
f = open('m2.txt') 
m2 = [line.replace("\n\n", "").split() for line in f]'''
'''a = input("Логин: ")
print (f'Ваш ник: {a}-123')
print (m1, m2, sep = '\n')'''
'''r1=len(m1)
c1=len(m1[0])
r2=len(m2)#3
c2=len(m2[0])#4
#os.system("pause")
def matrixpermutation(m2,z):
    m2[z], m2[z+1] = m2[z+1], m2[z]
    return m2
def matrixgaus(r2,c2):
    t=1 
    z=0
    m2=[]
    with open("m2.txt") as f:
        for line in f:
            m2.append([int(x) for x in line.split()])
    if m2[z][z] != 0 :
        for z in range(r2):
            for i in range(z+1, r2):
                for j in range(1, c2):
                    m2[i][j]=((m2[z][z]*m2[i][j])-(m2[i][z]*m2[z][j]))
                m2[i][z]=0
    else:
        matrixpermutation(m2,z)
        matrixgaus(r2,c2)
    return m2

with open('m2g.txt', 'w') as testfile:
    for row in matrixgaus(r2,c2):
        testfile.write(' '.join([str(a) for a in row]) + '\n')
def matrixtrans(r1,c1,m1):
    m4 = [[0]*r1 for _ in range(c1)]
    for i in range(r1):
        for j in range(c1):
            m4[j][i] = m1[i][j]
    return m4

print(matrixtrans(r1,c1,m1))
with open('m4.txt', 'w') as testfile:
    for row in matrixtrans(r1,c1,m1):
        testfile.write(' '.join([str(a) for a in row]) + '\n')
def matrixmult(r1,r2,c1,с2):
    m1 = []
    with open("m1.txt") as f:
        for line in f:
            m1.append([int(x) for x in line.split()])
    m2 = []
    with open("m2.txt") as f:
        for line in f:
            m2.append([int(x) for x in line.split()])
    print (m1,m2)
    s=0
    t=[]
    m3=[]
    if r2!=c1:
        print ('Матрицы не могут быть пермножены')
    else:
        for z in range(0,r1):
            for j in range(0,c2):
                for i in range(0,c1):
                    s=s+m1[z][i]*m2[i][j]
                t.append(s)
                s=0
            m3.append(t)
            t=[]
    return m3
print (matrixmult(r1,r2,c1,c2))
with open('m3.txt', 'w') as testfile:
    for row in matrixmult(r1,r2,c1,c2):
        testfile.write(' '.join([str(a) for a in row]) + '\n')
os.system("pause")
'''