import pygame
from random import randrange 

RES = 800
SIZE = 20

x, y = randrange(SIZE, RES - SIZE, SIZE), randrange(SIZE, RES - SIZE, SIZE)
apple = randrange(SIZE, RES - SIZE, SIZE), randrange(SIZE, RES - SIZE, SIZE)
length = 1
snake = [(x,y)]
dx, dy = 0, 0
fps = 60
dirs = {'W': True, 'S': True, 'A': True, 'D': True, }
score = 0
speed_count, snake_speed = 0, 9
death_snake = False

def close_game():
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            exit()

def restart_game():
    if not key[pygame.K_SPACE]:
        surface.blit(img, (0, 0))

pygame.init()
surface = pygame.display.set_mode([RES, RES])
clock = pygame.time.Clock()
font_score = pygame.font.SysFont('Arial', 26, bold = True)
font_end = pygame.font.SysFont('Arial', 66, bold = True)
img = pygame.image.load('bg2.jpg').convert()


while True:
    key = pygame.key.get_pressed()
    surface.blit(img, (0, 0))

    # рисунок змейки и яблока
    [pygame.draw.rect(surface, pygame.Color('palegreen'), (i, j, SIZE - 1, SIZE - 1)) for i, j in snake]
    pygame.draw.rect(surface, pygame.Color('maroon'), (*apple, SIZE, SIZE))
    # показать счёт
    render_score = font_score.render(f'SCORE: {score}', 1, pygame.Color('orange'))
    surface.blit(render_score, (5, 5))
    # движение змейки
    speed_count += 1
    if not speed_count % snake_speed:
        x += dx * SIZE
        y += dy *SIZE
        snake.append((x, y))
        snake = snake[-length:]
    # поедание яблока
    if snake[-1] == apple:
        apple = randrange(SIZE, RES - SIZE, SIZE), randrange(SIZE, RES - SIZE, SIZE)
        length += 1
        score += 1
        snake_speed -= 1
        snake_speed = max(snake_speed, 4) 

    # конец игры
    if not death_snake:
        if x < 0 or x > RES - SIZE or y < 0 or y > RES - SIZE or len(snake) != len(set(snake)):
            death_snake = True
        

    if death_snake:
        render_end = font_end.render('Game over, press space', 1, pygame.Color('purple'))
        surface.blit(render_end, (RES // 2 - 300, RES // 3))
        # speed_count, snake_speed = 0, 9

        if key[pygame.K_SPACE]:
            death_snake = False
            length = 1
            x, y = randrange(SIZE, RES - SIZE, SIZE), randrange(SIZE, RES - SIZE, SIZE)
            score = 0
            speed_count, snake_speed = 0, 9
            dx, dy = 0, 0
        

        

    pygame.display.flip()
    clock.tick(fps)

    close_game()
    # управление
    if key[pygame.K_w]:
        if dirs['W']:
            dx, dy = 0, -1
            dirs = {'W': True, 'S': False, 'A': True, 'D': True, }
    elif key[pygame.K_s]:
        if dirs['S']:
            dx, dy = 0, 1
            dirs = {'W': False, 'S': True, 'A': True, 'D': True, }
    elif key[pygame.K_a]:
        if dirs['A']:
            dx, dy = -1, 0
            dirs = {'W': True, 'S': True, 'A': True, 'D': False, }
    elif key[pygame.K_d]:
        if dirs['D']:
            dx, dy = 1, 0
            dirs = {'W': True, 'S': True, 'A': False, 'D': True, }